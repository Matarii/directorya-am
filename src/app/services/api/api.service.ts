import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { map, take, tap } from 'rxjs/operators';

import { environment } from '../../../environments/environment';

const API_URL = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})

export class ApiService {

  constructor(private httpClient: HttpClient) 
  {
    // this.setHeaderOptions();
  }

  httpOptions = {}

  setHeaderOptions() {
    console.log('setHeaderOptions launched')
    this.httpOptions = {
      headers: new HttpHeaders({
        // 'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('token')
      })
    }
  }


  get(url) {
    return this.httpClient.get(API_URL+url);
  }

  post(url, data): any {
    return this.httpClient.post(API_URL + url, data).pipe(tap(data => console.log('data', data)));
  }

  delete(url):any {
    return this.httpClient.delete(API_URL + url).pipe(tap(data => console.log('data', data)));
  }

}