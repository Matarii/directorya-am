import { Component } from '@angular/core';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsMapTypeId,
  GoogleMapsEvent,
  GoogleMapOptions,
  CameraPosition,
  MarkerOptions,
  Marker,
  Environment
} from '@ionic-native/google-maps';
import { ActionSheetController, Platform, AlertController } from '@ionic/angular';
import { ApiService } from '../services/api/api.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  map: GoogleMap;
  myLatitude;
  myLongitude;

  constructor(
    public alertController: AlertController,
    public actionCtrl: ActionSheetController,
    private platform: Platform,
    private api: ApiService,
    private geolocation: Geolocation,
  ) {
    if (this.platform.is('cordova')) {
      this.loadMap();
    }
  }

  loadMap() {
    this.geolocation.getCurrentPosition().then((resp) => {
      this.myLatitude = resp.coords.latitude;
      this.myLongitude = resp.coords.longitude;

      this.map = GoogleMaps.create('map_canvas', {
        camera: {
          target: {
            lat: this.myLatitude,
            lng: this.myLongitude
          },
          zoom: 18,
          tilt: 30
        }
      });

      this.map.addMarkerSync({
        title: 'Ma Position',
         icon: 'blue',
        animation: 'DROP',
        position: {
          lat: this.myLatitude,
          lng: this.myLongitude
        }
      });

      this.getBusinesses();

     }).catch((error) => {
       console.log('Error getting location', error);
     });
     
     let watch = this.geolocation.watchPosition();
     watch.subscribe((data) => {
      // data can be a set of coordinates, or an error (if an error occurred).
      // data.coords.latitude
      // data.coords.longitude
    });
  }

  placeMarker(markerTitle: string) {
    const marker: Marker = this.map.addMarkerSync({
       title: markerTitle,
       icon: 'red',
       animation: 'DROP',
       position: this.map.getCameraPosition().target
    });
  }

  async addMarker() {
    const alert = await this.alertController.create({
      header: 'Ajouter un emplacement',
      inputs: [
        {
          name: 'title',
          type: 'text',
          placeholder: 'Le titre'
        }
      ],
      buttons: [
        {
          text: 'Annuler',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ajouter',
          handler: data => {
            console.log('Titre: ' + data.title);
            this.placeMarker(data.title);
          }
        }
      ]
    });
    await alert.present();
  }

  getBusinesses() {
    let coord = {
      latitude: this.myLatitude,
      longitude: this.myLongitude,
      outer_radius: 1000
    }

    this.api.post('businessesByPositionFence', coord).subscribe(data => {
      data.forEach(item => {
        this.addMarkerToBusiness(item);
      });
    })
  }

  addMarkerToBusiness(item) {
    this.map.addMarkerSync({
      title: item.name,
       icon: 'red',
      animation: 'DROP',
      position: {
        lat: item.latitude,
        lng: item.longitude
      }
    });
  }
 
}
